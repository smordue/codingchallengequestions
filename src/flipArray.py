# Given a one dimensional array of data write a function that returns
# a new array with the data reversed. Don't just use the reverse
# function that is built into your environment.

testArray = ["Bobcat", "Lizard", "Tree"]

def flipArray(inputArray):
    lengthArray = len(inputArray)
    i=lengthArray
    outputArray = [None] * lengthArray
    for item in inputArray:
        i=i-1
        outputArray[i] = item
    return outputArray

print(flipArray(testArray))