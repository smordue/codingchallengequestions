# Given a one dimensional array of data write a function that returns
# M random elements of that array. Each element must also be from a
# different position in the array.

import random

def returnedNums2(numberM, numberN):
    count = 0
    listOfNums = []
    while count < numberM:
        currRand = random.randint(1, numberN)
        listOfNums.append(currRand)
        listOfNums = list(set(listOfNums))
        count = len(listOfNums)
    return listOfNums

def randomElements(inputArray):
    length = len(inputArray)
    numberM = input("Enter target number of rands M of array: ")
    numberM = int(numberM)
    indicesArray = returnedNums2(numberM,len(inputArray)-1)
    print(indicesArray)
    outputArray = []
    i=0
    while i<len(indicesArray):
        outputArray.append(inputArray[indicesArray[i]])
        i = i+1

    return outputArray

testArray1 = ["Gumdrop", "Candy Cane", "Reeses", "BonBon", "Sucker", "34", "56", "bubba", "elephant"]
print(randomElements(testArray1))