# Write a quick and dirty program (not just a standalone function) to
# print a count of all the different "words" in a text file. Use any
# definition of word that makes logical sense or makes your job easy.
# The output might look like this:
# 17 a
# 14 the
# 9 of
# 9 in
# 8 com
# 7 you
# 7 that
# 7 social
# 6 to
# ...
# For this input file, the word "a" occurred 17 times, "the" 14
# times, etc.