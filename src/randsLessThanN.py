# Write a function that returns M random non-negative integers less
# than some value N. Each integer must also be unique.

import random

def returnedNums():
    numberM = input("Enter target number of rands M: ")
    numberM = int(numberM)
    numberN = input("Enter less than target N: ")
    numberN = int(numberN)
    count = 0
    listOfNums = []
    while count < numberM:
        currRand = random.randint(1, numberN)
        listOfNums.append(currRand)
        listOfNums = list(set(listOfNums))
        count = len(listOfNums)
    return listOfNums

print(returnedNums())